﻿Public Class Form1

    Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click
        Dim item As String = lb_l.SelectedItem
        If item <> "" Then
            lb_r.Items.Add(item)
            lb_l.Items.Remove(item)
        End If

    End Sub

    Private Sub btn_remove_Click(sender As Object, e As EventArgs) Handles btn_remove.Click
        Dim item As String = lb_r.SelectedItem
        If item <> "" Then
            lb_l.Items.Add(item)
            lb_r.Items.Remove(item)
        End If
    End Sub

    Private Sub btn_addAll_Click(sender As Object, e As EventArgs) Handles btn_addAll.Click
        'For Each item As String In lb_l.Items
        'lb_r.Items.Add(item)
        'Next
        lb_r.Items.AddRange(lb_l.Items)
        lb_l.Items.Clear()
    End Sub

    Private Sub btn_removeAll_Click(sender As Object, e As EventArgs) Handles btn_removeAll.Click
        'For Each item As String In lb_r.Items
        'lb_l.Items.Add(item)
        'Next
        lb_l.Items.AddRange(lb_r.Items)
        lb_r.Items.Clear()
    End Sub

    Private Sub ComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox.SelectedIndexChanged
        Label.Text = ComboBox.SelectedItem
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Label.Text = ""
    End Sub

    Private Sub Label_Click(sender As Object, e As EventArgs) Handles Label.Click
        If sender.Text <> "" Then
            System.Diagnostics.Process.Start(sender.Text)
        End If
    End Sub
End Class
