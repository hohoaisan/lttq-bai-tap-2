﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lb_l = New System.Windows.Forms.ListBox()
        Me.lb_r = New System.Windows.Forms.ListBox()
        Me.btn_add = New System.Windows.Forms.Button()
        Me.btn_addAll = New System.Windows.Forms.Button()
        Me.btn_removeAll = New System.Windows.Forms.Button()
        Me.btn_remove = New System.Windows.Forms.Button()
        Me.ComboBox = New System.Windows.Forms.ComboBox()
        Me.Label = New System.Windows.Forms.LinkLabel()
        Me.SuspendLayout()
        '
        'lb_l
        '
        Me.lb_l.DisplayMember = "aa"
        Me.lb_l.FormattingEnabled = True
        Me.lb_l.Items.AddRange(New Object() {"Javascript", "Python", "Java", "Swift", "Visual Basic", "C#", "C++"})
        Me.lb_l.Location = New System.Drawing.Point(12, 12)
        Me.lb_l.Name = "lb_l"
        Me.lb_l.Size = New System.Drawing.Size(179, 186)
        Me.lb_l.TabIndex = 0
        '
        'lb_r
        '
        Me.lb_r.FormattingEnabled = True
        Me.lb_r.Location = New System.Drawing.Point(260, 12)
        Me.lb_r.Name = "lb_r"
        Me.lb_r.Size = New System.Drawing.Size(179, 186)
        Me.lb_r.TabIndex = 1
        '
        'btn_add
        '
        Me.btn_add.Location = New System.Drawing.Point(206, 38)
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Size = New System.Drawing.Size(35, 22)
        Me.btn_add.TabIndex = 2
        Me.btn_add.Text = ">"
        Me.btn_add.UseVisualStyleBackColor = True
        '
        'btn_addAll
        '
        Me.btn_addAll.Location = New System.Drawing.Point(206, 75)
        Me.btn_addAll.Name = "btn_addAll"
        Me.btn_addAll.Size = New System.Drawing.Size(35, 22)
        Me.btn_addAll.TabIndex = 3
        Me.btn_addAll.Text = ">>"
        Me.btn_addAll.UseVisualStyleBackColor = True
        '
        'btn_removeAll
        '
        Me.btn_removeAll.Location = New System.Drawing.Point(206, 152)
        Me.btn_removeAll.Name = "btn_removeAll"
        Me.btn_removeAll.Size = New System.Drawing.Size(35, 22)
        Me.btn_removeAll.TabIndex = 5
        Me.btn_removeAll.Text = "<<"
        Me.btn_removeAll.UseVisualStyleBackColor = True
        '
        'btn_remove
        '
        Me.btn_remove.Location = New System.Drawing.Point(206, 115)
        Me.btn_remove.Name = "btn_remove"
        Me.btn_remove.Size = New System.Drawing.Size(35, 22)
        Me.btn_remove.TabIndex = 4
        Me.btn_remove.Text = "<"
        Me.btn_remove.UseVisualStyleBackColor = True
        '
        'ComboBox
        '
        Me.ComboBox.FormattingEnabled = True
        Me.ComboBox.Items.AddRange(New Object() {"http://ute.udn.vn", "https://google.com", "http://github.com"})
        Me.ComboBox.Location = New System.Drawing.Point(490, 89)
        Me.ComboBox.Name = "ComboBox"
        Me.ComboBox.Size = New System.Drawing.Size(220, 21)
        Me.ComboBox.TabIndex = 6
        '
        'Label
        '
        Me.Label.AutoSize = True
        Me.Label.Location = New System.Drawing.Point(562, 139)
        Me.Label.Name = "Label"
        Me.Label.Size = New System.Drawing.Size(85, 13)
        Me.Label.TabIndex = 7
        Me.Label.TabStop = True
        Me.Label.Text = "Chọn để hiển thị"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label)
        Me.Controls.Add(Me.ComboBox)
        Me.Controls.Add(Me.btn_removeAll)
        Me.Controls.Add(Me.btn_remove)
        Me.Controls.Add(Me.btn_addAll)
        Me.Controls.Add(Me.btn_add)
        Me.Controls.Add(Me.lb_r)
        Me.Controls.Add(Me.lb_l)
        Me.Name = "Form1"
        Me.Text = "Sản_51"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lb_l As ListBox
    Friend WithEvents lb_r As ListBox
    Friend WithEvents btn_add As Button
    Friend WithEvents btn_addAll As Button
    Friend WithEvents btn_removeAll As Button
    Friend WithEvents btn_remove As Button
    Friend WithEvents ComboBox As ComboBox
    Friend WithEvents Label As LinkLabel
End Class
